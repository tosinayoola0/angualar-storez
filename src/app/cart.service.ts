import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items = [];

  addToCart(product){
    return this.items.push(product)
  };

  viewCart(){
    return this.items
  };
  clearCart(){
    this.items  =[]
    return this.items;
  }
  constructor() { }

}