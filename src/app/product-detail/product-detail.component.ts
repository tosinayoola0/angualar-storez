import { Component, OnInit } from '@angular/core';
import {CartService} from '../cart.service'
import {ActivatedRoute} from '@angular/router'
import {products} from '../products'

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  product;
  

  addProduct(product){
      this.CartService.addToCart(product);
      alert('cool, your product have been added to cart ')
  }

  
  constructor(
    private route: ActivatedRoute,
    private CartService: CartService
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      this.product=products[+params.get('productId')]
    })
  }

}